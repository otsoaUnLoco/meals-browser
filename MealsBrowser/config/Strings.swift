//
//  Strings.swift
//  ios-playground
//
//  Created by Dawid Bota on 05/04/2022.
//

import Foundation

struct Strings {
    static let mealsTitle: String.LocalizationValue = "meals_view_title"
    static let mealsSearchPlaceholder: String.LocalizationValue = "meals_search_placeholder"
}

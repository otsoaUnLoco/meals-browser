//
//  Constants.swift
//  ios-playground
//
//  Created by Dawid Bota on 04/04/2022.
//

import Foundation

struct Constants {
    struct API {
        static let scheme = "https"
        static let host = "themealdb.com"
        static let basePath = "/api/json/v1/1/"
    }
}

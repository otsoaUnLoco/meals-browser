//
//  Meals+Injections.swift
//  ios-playground
//
//  Created by Dawid Bota on 05/04/2022.
//

import Foundation
import Resolver

extension Resolver: ResolverRegistering {
    public static func registerAllServices() {
        register { MealsDataSource() }
        register { MealsRepository(mealsDataSource: resolve()) }
        register { (_, args) in MealsViewModel(mealsRepository: resolve(), delegate: args()) }
    }
}

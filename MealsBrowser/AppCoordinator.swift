//
//  AppCoordinator.swift
//  MealsBrowser
//
//  Created by Dawid Bota on 06/04/2022.
//

import Foundation
import UIKit

class AppCoordinator: Coordinator {
    private let window: UIWindow
    private lazy var navController = UINavigationController()
    private lazy var mealsCoordinator = MealsCoordinator(navController: navController)
    
    internal init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        window.rootViewController = navController
        window.makeKeyAndVisible()
        mealsCoordinator.start()
    }
}

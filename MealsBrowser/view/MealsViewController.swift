//
//  MealsViewController.swift
//  ios-playground
//
//  Created by Dawid Bota on 04/04/2022.
//

import Combine
import Foundation
import Kingfisher
import UIKit
import Resolver

class MealsViewController: UIViewController {
    typealias DataSource = UICollectionViewDiffableDataSource<Int, Meal>
    typealias Snapshot = NSDiffableDataSourceSnapshot<Int, Meal>

    private let viewModel: MealsViewModel
    private lazy var contentView = MealsView()
    private var bindings = Set<AnyCancellable>()
    private lazy var dataSource: DataSource = initDataSource()
    
    internal init(viewModel: MealsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = contentView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = String(localized: Strings.mealsTitle)
        setupReminderList()
        bindViewToViewModel()
        bindViewModelToView()
    }

    private func bindViewToViewModel() {
        contentView.searchMeals.searchTextField.textPublisher
            .removeDuplicates()
            .compactMap { $0 }
            .assign(to: \.query, on: viewModel)
            .store(in: &bindings)
    }

    private func bindViewModelToView() {
        viewModel.meals
            .subscribe(on: DispatchQueue.global())
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { completion in
                    switch completion {
                    case let .failure(error): print("Error \(error)")
                    case .finished: print("Publisher is finished")
                    }
                },
                receiveValue: { [weak self] in
                    var snapshot = Snapshot()
                    snapshot.appendSections([0])
                    snapshot.appendItems($0)
                    self?.dataSource.apply(snapshot)
                }
            )
            .store(in: &bindings)
    }

    private func setupReminderList() {
        contentView.mealsList.delegate = self
        contentView.mealsList.dataSource = dataSource
    }
}

extension MealsViewController: UICollectionViewDelegate {
    func collectionView(_ collection: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.didSelectMeal(dataSource.itemIdentifier(for: indexPath)!)
        collection.deselectItem(at: indexPath, animated: true)
    }
}

extension MealsViewController {
    private func initDataSource() -> DataSource {
        let registration = contentView.createCellRegistration()
        return DataSource(collectionView: contentView.mealsList) { (
            collectionView: UICollectionView,
            indexPath: IndexPath,
            itemIdentifier: Meal
        ) in collectionView.dequeueConfiguredReusableCell(
                using: registration,
                for: indexPath,
                item: itemIdentifier
            )
        }
    }
}

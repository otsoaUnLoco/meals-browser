//
//  MealsDetailsView.swift
//  ios-playground
//
//  Created by Dawid Bota on 05/04/2022.
//

import Foundation
import UIKit

class MealDetailsView: UIView {
    lazy var scrollView = UIScrollView()
    lazy var mealImageView = UIImageView()
    lazy var instructionsView = UILabel()
    lazy var ingredientsView = UILabel()

    internal init() {
        super.init(frame: .zero)

        backgroundColor = .white
        addSubviews()
        setupViews()
        setupConstraints()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func addSubviews() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(scrollView)
        [mealImageView, instructionsView, ingredientsView]
            .forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
                scrollView.addSubview($0)
            }
    }

    private func setupViews() {
        mealImageView.contentMode = .scaleToFill
        instructionsView.numberOfLines = 0
    }

    private func setupConstraints() {
        let margin = 8.0
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
            mealImageView.topAnchor.constraint(equalTo: scrollView.contentLayoutGuide.topAnchor),
            mealImageView.leadingAnchor
                .constraint(equalTo: scrollView.contentLayoutGuide.leadingAnchor),
            mealImageView.trailingAnchor
                .constraint(equalTo: scrollView.contentLayoutGuide.trailingAnchor),
            mealImageView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.4),
            mealImageView.widthAnchor.constraint(equalTo: widthAnchor),
            instructionsView.topAnchor.constraint(equalTo: mealImageView.bottomAnchor, constant: 8),
            instructionsView.leadingAnchor.constraint(
                equalTo: scrollView.contentLayoutGuide.leadingAnchor,
                constant: margin
            ),
            instructionsView.trailingAnchor.constraint(
                equalTo: scrollView.contentLayoutGuide.trailingAnchor,
                constant: -margin
            ),
            ingredientsView.topAnchor.constraint(
                equalTo: instructionsView.bottomAnchor,
                constant: 8
            ),
            ingredientsView.leadingAnchor.constraint(
                equalTo: scrollView.contentLayoutGuide.leadingAnchor,
                constant: margin
            ),
            ingredientsView.trailingAnchor.constraint(
                equalTo: scrollView.contentLayoutGuide.trailingAnchor,
                constant: -margin
            ),
            ingredientsView.bottomAnchor
                .constraint(equalTo: scrollView.contentLayoutGuide.bottomAnchor),
        ])
    }
}

//
//  MealsView.swift
//  ios-playground
//
//  Created by Dawid Bota on 04/04/2022.
//

import Foundation
import UIKit
import Kingfisher

class MealsView: UIView {
    lazy var searchMeals: UISearchBar = .init()
    lazy var mealsList: UICollectionView = initCollectionView()

    init() {
        super.init(frame: .zero)
        addSubviews()
        setupViews()
        setupConstraints()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initCollectionView() -> UICollectionView {
        let collection = UICollectionView(frame: frame, collectionViewLayout: listLayout())
        return collection
    }

    private func listLayout() -> UICollectionViewCompositionalLayout {
        let listConfiguration = UICollectionLayoutListConfiguration(appearance: .plain)
        return UICollectionViewCompositionalLayout.list(using: listConfiguration)
    }

    private func addSubviews() {
        [searchMeals, mealsList].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            addSubview($0)
        }
    }
    
    private func setupViews() {
        searchMeals.placeholder = String(localized: Strings.mealsSearchPlaceholder)
    }

    private func setupConstraints() {
        NSLayoutConstraint.activate([
            searchMeals.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            searchMeals.leadingAnchor.constraint(equalTo: leadingAnchor),
            searchMeals.trailingAnchor.constraint(equalTo: trailingAnchor),
            mealsList.leadingAnchor.constraint(equalTo: leadingAnchor),
            mealsList.trailingAnchor.constraint(equalTo: trailingAnchor),
            mealsList.topAnchor.constraint(equalTo: searchMeals.bottomAnchor),
            mealsList.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
}

extension MealsView {
    func createCellRegistration() -> UICollectionView
        .CellRegistration<UICollectionViewListCell, Meal>
    {
        return .init { (cell: UICollectionViewListCell, _: IndexPath, meal: Meal) in
            let imageSize = CGSize(width: 48, height: 48)
            var contentConfiguration = cell.defaultContentConfiguration()
            contentConfiguration.text = meal.name
            contentConfiguration.image = UIImage(named: "placeholder")
            contentConfiguration.imageProperties.maximumSize = imageSize
            cell.contentConfiguration = contentConfiguration
            cell.accessories = [.disclosureIndicator()]

            let imageResource = ImageResource(downloadURL: URL(string: meal.thumbnail)!)
            KingfisherManager.shared.retrieveImage(with: imageResource, options: [
                .processor(DownsamplingImageProcessor(size: imageSize)),
            ]) { result in
                switch result {
                case let .success(value):
                    contentConfiguration.image = value.image
                    cell.contentConfiguration = contentConfiguration
                case let .failure(error):
                    print("Error: \(error)")
                }
            }
        }
    }
}

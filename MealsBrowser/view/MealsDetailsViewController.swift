//
//  MealsDetailsViewController.swift
//  ios-playground
//
//  Created by Dawid Bota on 05/04/2022.
//

import Foundation
import Kingfisher
import UIKit

class MealsDetailsViewController: UIViewController {
    private let meal: Meal
    private lazy var contentView = MealDetailsView()

    internal init(meal: Meal) {
        self.meal = meal
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = contentView
    }

    override func viewDidLoad() {
        title = meal.name
        bindViews()
    }

    private func bindViews() {
        contentView.mealImageView.kf.setImage(with: URL(string: meal.thumbnail))
        contentView.instructionsView.text = meal.instructions
    }
}

//
//  MealsViewModel.swift
//  ios-playground
//
//  Created by Dawid Bota on 04/04/2022.
//

import Foundation
import Combine

class MealsViewModel {
    private let mealsRepository: MealsRepository
    private weak var delegate: MealsViewModelCoordinatorDelegate?
    @Published var query: String = ""
    
    private(set) lazy var meals: AnyPublisher<[Meal], Error> = $query
        .debounce(for: .milliseconds(500), scheduler: DispatchQueue.global())
        .flatMap { [self] in
            mealsRepository.searchMeals(by: $0)
        }
        .eraseToAnyPublisher()
    
    internal init(mealsRepository: MealsRepository, delegate: MealsViewModelCoordinatorDelegate) {
        self.mealsRepository = mealsRepository
        self.delegate = delegate
    }
    
    func didSelectMeal(_ meal: Meal) {
        delegate?.goToDetails(meal: meal)
    }
}

protocol MealsViewModelCoordinatorDelegate: AnyObject {
    func goToDetails(meal: Meal)
}

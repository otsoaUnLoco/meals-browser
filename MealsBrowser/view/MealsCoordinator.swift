//
//  MealsCoordinator.swift
//  MealsBrowser
//
//  Created by Dawid Bota on 06/04/2022.
//

import Foundation
import UIKit
import Resolver

class MealsCoordinator: Coordinator, Resolving {
    private let navController: UINavigationController
    
    internal init(navController: UINavigationController) {
        self.navController = navController
    }
    
    func start() {
        let mealsController = MealsViewController(viewModel: resolver.resolve(args: self))
        navController.view.backgroundColor = .white
        navController.viewControllers = [mealsController]
    }
}

extension MealsCoordinator : MealsViewModelCoordinatorDelegate {
    func goToDetails(meal: Meal) {
        let detailsController = MealsDetailsViewController(meal: meal)
        navController.show(detailsController, sender: self)
    }
}

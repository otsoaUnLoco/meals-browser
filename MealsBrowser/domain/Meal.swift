//
//  Meal.swift
//  ios-playground
//
//  Created by Dawid Bota on 04/04/2022.
//

import Foundation

struct Meal: Hashable {
    let id: Int
    let name: String
    let thumbnail: String
    let instructions: String
    let tags: [String]
    let ingredients: [Ingredient]
}

struct Ingredient: Hashable {
    let name: String
    let count: String
}

//
//  MealNetworkResponse.swift
//  ios-playground
//
//  Created by Dawid Bota on 04/04/2022.
//

import Foundation

struct MealNetworkResponse : Codable {
    var meals: [MealNetworkModel]? = nil
}

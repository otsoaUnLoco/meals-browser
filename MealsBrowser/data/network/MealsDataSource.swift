//
//  MealsDataSource.swift
//  ios-playground
//
//  Created by Dawid Bota on 04/04/2022.
//

import Foundation
import Alamofire
import Combine

class MealsDataSource {
    func search(by name: String) -> AnyPublisher<[MealNetworkModel], Error> {
        return AF.request(RequestUrlFactory.searchByName(name))
            .validate()
            .publishDecodable(type: MealNetworkResponse.self)
            .value()
            .mapError { $0 as Error }
            .map { $0.meals ?? [] }
            .eraseToAnyPublisher()
    }
}

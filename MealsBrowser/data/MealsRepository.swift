//
//  MealsRepository.swift
//  ios-playground
//
//  Created by Dawid Bota on 04/04/2022.
//

import Foundation
import Combine
import Alamofire

class MealsRepository {
    private let mealsDataSource: MealsDataSource
    
    internal init(mealsDataSource: MealsDataSource) {
        self.mealsDataSource = mealsDataSource
    }
    
    func searchMeals(by name: String) -> AnyPublisher<[Meal], Error> {
        return mealsDataSource.search(by: name)
            .map { (meals: [MealNetworkModel]) in
                meals.map { meal in meal.toMeal() }
            }
            .eraseToAnyPublisher()
    }
}

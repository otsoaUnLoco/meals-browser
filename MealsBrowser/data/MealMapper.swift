//
//  MealMapper.swift
//  ios-playground
//
//  Created by Dawid Bota on 04/04/2022.
//

import Foundation

extension MealNetworkModel {
    func toMeal() -> Meal {
        return Meal(
            id: Int(idMeal)!,
            name: strMeal,
            thumbnail: strMealThumb,
            instructions: strInstructions,
            tags: strTags?
                .replacingOccurrences(of: " ", with: "")
                .components(separatedBy: ",") ?? [],
            ingredients: getIngredients()
        )
    }

    private func getIngredients() -> [Ingredient] {
        let ingredients = [
            strIngredient1,
            strIngredient2,
            strIngredient3,
            strIngredient4,
            strIngredient5,
            strIngredient6,
            strIngredient7,
            strIngredient8,
            strIngredient9,
            strIngredient10,
            strIngredient11,
            strIngredient12,
            strIngredient13,
            strIngredient14,
            strIngredient15,
            strIngredient16,
            strIngredient17,
            strIngredient18,
            strIngredient19,
            strIngredient20,
        ]
        let measures = [
            strMeasure1,
            strMeasure2,
            strMeasure3,
            strMeasure4,
            strMeasure5,
            strMeasure6,
            strMeasure7,
            strMeasure8,
            strMeasure9,
            strMeasure10,
            strMeasure11,
            strMeasure12,
            strMeasure13,
            strMeasure14,
            strMeasure15,
            strMeasure16,
            strMeasure17,
            strMeasure18,
            strMeasure19,
            strMeasure20,
        ]
        return zip(ingredients, measures)
            .compactMap { (k: String?, v: String?) in
                guard let ingredient = k, let measure = v else {
                    return nil
                }
                return Ingredient(name: ingredient, count: measure)
            }
    }
}

//
//  UIViewController+extensions.swift
//  ios-playground
//
//  Created by Dawid Bota on 05/04/2022.
//

import Foundation
import UIKit

protocol RegistrableUICollection {
    associatedtype T: Hashable
    var collectionView: UICollectionView { get }
    var cellRegistration: UICollectionView.CellRegistration<UICollectionViewListCell, T> { get }
}

extension RegistrableUICollection {
    func createDataSource() -> UICollectionViewDiffableDataSource<Int, T> {
        let registration = cellRegistration
        return UICollectionViewDiffableDataSource(collectionView: collectionView) { (
            collectionView: UICollectionView,
            indexPath: IndexPath,
            itemIdentifier: T
        ) in collectionView.dequeueConfiguredReusableCell(
                using: registration,
                for: indexPath,
                item: itemIdentifier
            )
        }
    }
}

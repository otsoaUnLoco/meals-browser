//
//  RequestUrlFactory.swift
//  ios-playground
//
//  Created by Dawid Bota on 04/04/2022.
//

import Foundation

class RequestUrlFactory {
    static let searchEndpoint = "search.php"
    static let searchQuery = "s"

    static func searchByName(_ name: String) -> URL {
        return buildUrl(
            endpoint: searchEndpoint,
            query: [URLQueryItem(name: searchQuery, value: name)]
        )!
    }

    private static func buildUrl(endpoint: String, query: [URLQueryItem]? = nil) -> URL? {
        var components = URLComponents()
        components.scheme = Constants.API.scheme
        components.host = Constants.API.host
        components.path = "\(Constants.API.basePath)\(endpoint)"
        components.queryItems = query
        return components.url
    }
}

//
//  Coordinator.swift
//  MealsBrowser
//
//  Created by Dawid Bota on 06/04/2022.
//

import Foundation
import UIKit

protocol Coordinator {
    func start()
}
